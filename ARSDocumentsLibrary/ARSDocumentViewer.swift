//
//  ARSDocumentViewer.swift
//  ARSDocumentsLibrary
//
//  Created by localadmin on 7/31/18.
//  Copyright © 2018 localadmin. All rights reserved.
//

import UIKit

public class ARSDocumentViewer: UIViewController {

    public override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    public convenience init() {
        let documentBundle = Bundle(for: ARSDocumentViewer.self)
        self.init(nibName: "ARSDocumentViewer", bundle: documentBundle)
    }
}
