//
//  ViewController.swift
//  ARSDocuments
//
//  Created by localadmin on 7/31/18.
//  Copyright © 2018 localadmin. All rights reserved.
//

import UIKit

import ARSDocumentsLibrary

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func viewDocuments(_ sender: UIButton) {
        let documentViewer = ARSDocumentViewer()
        navigationController?.pushViewController(documentViewer, animated: true)
    }
}

